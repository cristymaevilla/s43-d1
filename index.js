// mock data
let posts =[];
// id property
let count = 1;


// Add post data---------------
document.querySelector('#form-add-post').addEventListener('submit',(e)=>{
	// preventDefault-prevents page from loading
	e.preventDefault();



	posts.push({
		id: count,
		title:document.querySelector('#txt-title').value,
		body:document.querySelector('#txt-body').value
	})
	count++;
	showPosts(posts);
	alert('Successfully added post');
	console.log(posts);
})

// show posts--------------
const showPosts = (posts)=> {
	let postEntries ='';
	posts.forEach((post)=>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})
	document.querySelector('#div-post-entries').innerHTML=postEntries;
}
// Edit Post
const editPost=(id)=>{

	let title=document.querySelector(`#post-title-${id}`).innerHTML;
	let body=document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value= id;
	document.querySelector('#txt-edit-title').value=title;
	document.querySelector('#txt-edit-body').value=body;
}
// Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
	e.preventDefault();

	for(let i=0; i<posts.length; i++){
		if(posts[i].id.toString()===document.querySelector('#txt-edit-id').value)
		{
			posts[i].title=document.querySelector('#txt-edit-title').value;
			posts[i].body=document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated');
			break;
		}


	}
})

// 

const dPost = (posts)=> {
	let postEntries ='';
	posts.forEach((post)=>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})
	document.querySelector('#div-post-entries').innerHTML.style.display="none";
}
const deletePost=(id)=>{
	/*console.log(posts[0].title);
	console.log(document.querySelector(`#post-title-${id}`).innerHTML)*/

	for(let x=0; x<posts.length; x++){
		if(posts[x].title===document.querySelector(`#post-title-${id}`).innerHTML)
		{	console.log(posts[x])
			let toDeletePost=posts[x];
			let id= posts[x].id
			console.log(id);
			posts.splice(toDeletePost,1);
			// toDeletePost.style.display='none';
			dPost(toDeletePost)
			
			


			console.log(posts)
			break;
		}
	}
}

